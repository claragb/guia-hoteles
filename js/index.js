$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover({
        data-container: 'body'
    });
    $(".carousel").carousel({
        interval: 2000
    });
});
